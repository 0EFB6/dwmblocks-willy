//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/								/*Update Interval*/	/*Update Signal*/
	/*{"", "free -h | grep 'Mem' | awk '{print $3}'",	1,		0},*/
	/*{"", "date '+%d %a %H:%M'",					2,		0},*/
	{"", 		"sh /opt/dwmblocks-willy/scripts/pac-update.sh",     30,     			0}, 
    {"", 		"sh /opt/dwmblocks-willy/scripts/cpu.sh",            2,       			0},
    {"",        "sh /opt/dwmblocks-willy/scripts/memory.sh",	     2,		            0},
	{"", 		"date '+%b %d (%a) %H:%M'",	5,					0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "| ";
static unsigned int delimLen = 5;